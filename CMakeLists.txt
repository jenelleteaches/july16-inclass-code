cmake_minimum_required(VERSION 3.14)
project(TuesdayJuly16_Code C)

set(CMAKE_C_STANDARD 99)

add_executable(TuesdayJuly16_Code main.c BubbleSort.c BubbleSort-v2.c BubbleSort-v3.c RecursionExample-Fibonacci.c)
add_executable(BubbleSort BubbleSort.c)
add_executable(BubbleSort-v2 BubbleSort-v2.c)
add_executable(BubbleSort-v3 BubbleSort-v3.c)
add_executable(RecursionExample-Fibonacci RecursionExample-Fibonacci.c)