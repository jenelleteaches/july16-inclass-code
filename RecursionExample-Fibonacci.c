//
// Created by Parrot on 2019-07-16.
//
#include <stdlib.h>
#include <stdio.h>

int fib(int n)
{
    int sum = 0;

    if (n == 1) {
        sum = 1;
        return sum;
    }
    if (n == 2) {
        sum = 1;
        return sum;
    }
    if (n > 2) {
        sum = fib(n - 1) + fib(n - 2);
    }
}


int main() {
    //int result = fib(9);
    printf("Result: %d\n", fib(9));
    return 0;
}