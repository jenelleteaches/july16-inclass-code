//
// Created by Parrot on 2019-07-16.
//
#include <stdlib.h>
#include <stdio.h>

int ARRAY_SIZE = 7;

// A helper function to print out content of array
void printArray(int array[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main() {
    // 1. create your unsorted array
    int array[] = {120, 40, 50, 30 , 80, 70, 100};

    // create a variable to keep track of iterations
    int count = 0;

    // 2. print your array
    printf("Unsorted array is: \n");
    printArray(array);


    // 3. Do the slow, stupid bubble sort
    // (This version is very inefficient)
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("Current iteration is i = %d\n", i);
        for (int j = 0; j < ARRAY_SIZE-1-i; j++) {
            int curr = array[j];
            int next = array[j + 1];

            printf("+ Comparing %d and %d\n", curr, next);
            // compare and see if you need to swap
            if (curr > next) {
                printf("------- Doing a swap!\n");
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }

            // track how many times this loop runs
            count = count + 1;
        }
        printf("---------\n");

    }

    // 3. print your array
    printf("Sorted array is: \n");
    printArray(array);


    // 4. output the total number of iterations
    printf("Total iterations: %d\n", count);

    return 0;
}
